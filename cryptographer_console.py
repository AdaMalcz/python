# -*- coding: utf-8 -*-
"""
Created on Mon May  6 16:45:55 2019

@author: Rege
"""

import ciphers

#Starting window
def starting_message():
    print("Choose your cipher:")
    print("\t [1] Caesar Cipher")
    print("\t [2] Vigenere Cipher \n")
   
    response = input("Enter cipher name or corresponding number: ")
    if response == "1" or response.lower() == "caesar" or response.lower() == "caesar cipher":
        caesar_cipher()
    elif response == "2" or response.lower() == "vigenere" or response.lower() == "vigenere cipher":
        vigenere_cipher()
    else:
        print("No " + response + " cipher found. \n")
        starting_message()

#Cipher choice
def caesar_cipher():
    print()
    print("What do you want to do:")
    print("\t [1] Code message")
    print("\t [2] Decode message")
    print("\t [3] Show cipher info \n")

    response = input("Enter action name or corresponding number: ")
    if response == "1" or response.lower() == "code" or response.lower() == "code message":
        caesar_cipher_code()
    elif response == "2" or response.lower() == "decode" or response.lower() == "descode message":
        caesar_cipher_decode()
    elif response == "3" or response.lower() == "info" or response.lower() == "show cipher info":
        ciphers.caesar_info()
        caesar_cipher()
    else:
        print("No " + response + " action. \n")
        caesar_cipher()

def vigenere_cipher():
    print()
    print("What do you want to do:")
    print("\t [1] Code message")
    print("\t [2] Decode message")
    print("\t [3] Show cipher info \n")

    response = input("Enter action name or corresponding number: ")
    if response == "1" or response.lower() == "code" or response.lower() == "code message":
        vigenere_cipher_code()
    elif response == "2" or response.lower() == "decode" or response.lower() == "descode message":
        vigenere_cipher_decode()
    elif response == "3" or response.lower() == "info" or response.lower() == "show cipher info":
        ciphers.vigenere_info()
        vigenere_cipher()
    else:
        print("No " + response + " action. \n")
        vigenere_cipher()

#Actions
def caesar_cipher_code():
    status = "on"
    message = input("Enter your message: ")
    shift = input("Enter shift value: ")
    print()
    print("Coded message: " + ciphers.caesar_coder(message, int(shift)) + "\n")
    
    while status == "on":
        print("What do you want to do next?" )
        print("\t [1] Another message")
        print("\t [2] Change cipher")
        print("\t [3] Exit \n")
        response = input("Enter number: ")
        if response == "1":
            status = "off"
            caesar_cipher_code()
        elif response == "2":
            status = "off"
            starting_message()
        elif response == "3" or response.lower() == "exit":
            status = "off"
            print("See you next time cryptographer!")
        else:
            print("No " + response + " action. \n")
        
def caesar_cipher_decode():
    status = "on"
    message = input("Enter coded message: ")
    shift = input("Enter shift value: ")
    print()
    print("Decoded message: " + ciphers.caesar_decoder(message, int(shift)) + "\n")
    
    while status == "on":
        print("What do you want to do next?" )
        print("\t [1] Another message")
        print("\t [2] Change cipher")
        print("\t [3] Exit \n")
        response = input("Enter number: ")
        if response == "1":
            status = "off"
            caesar_cipher_decode()
        elif response == "2":
            status = "off"
            starting_message()
        elif response == "3" or response.lower() == "exit":
            status = "off"
            print("See you next time cryptographer!")
        else:
            print("No " + response + " action. \n")
            
def vigenere_cipher_code():
    status = "on"
    message = input("Enter your message: ")
    keyword = input("Enter your keyword: ")
    print()
    print("Coded message: " + ciphers.vigenere_coder(message, keyword) + "\n")
    
    while status == "on":
        print("What do you want to do next?" )
        print("\t [1] Another message")
        print("\t [2] Change cipher")
        print("\t [3] Exit \n")
        response = input("Enter number: ")
        if response == "1":
            status = "off"
            vigenere_cipher_code()
        elif response == "2":
            status = "off"
            starting_message()
        elif response == "3" or response.lower() == "exit":
            status = "off"
            print("See you next time cryptographer!")
        else:
            print("No " + response + " action. \n")

def vigenere_cipher_decode():
    status = "on"
    message = input("Enter coded message: ")
    keyword = input("Enter keyword: ")
    print()
    print("Decoded message: " + ciphers.vigenere_decoder(message, keyword) + "\n")
    
    while status == "on":
        print("What do you want to do next?" )
        print("\t [1] Another message")
        print("\t [2] Change cipher")
        print("\t [3] Exit \n")
        response = input("Enter number: ")
        if response == "1":
            status = "off"
            vigenere_cipher_decode()
        elif response == "2":
            status = "off"
            starting_message()
        elif response == "3" or response.lower() == "exit":
            status = "off"
            print("See you next time cryptographer!")
        else:
            print("No " + response + " action. \n")

### PROGRAM BODY:
print("Welcome to Cryptographer v1.00 \n")
starting_message()