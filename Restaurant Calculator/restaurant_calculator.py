# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 14:48:01 2019

@author: Rege
"""

from datetime import time, datetime

class Menu:
  def __init__(self, name, items, start_time, end_time):
    self.name = name
    self.items = items
    self.start_time = start_time
    self.end_time = end_time
    
  def __str__(self):
    representation = "{name} menu available from {start} to {end}.".format(name=self.name, start=self.start_time.strftime("%I:%M-%p"), end=self.end_time.strftime("%I:%M-%p"))
    return representation
  
  def calculate_bill(self, purchased_items):
    self.total_price = 0
    for item in purchased_items:
      self.total_price += self.items[item]
    return self.total_price

class Franchise:
  def __init__(self, address, menus):
    self.address = address
    self.menus = menus
    
  def __str__(self):
    representation = "The address of restaurant is: {}.".format(self.address)
    return representation
  
  def available_menus(self):
    available_menus = []
    self.present_time = datetime.now().time()
    for menu in self.menus:
      if self.present_time <= menu.end_time and self.present_time >= menu.start_time:
        available_menus.append(str(menu))
    print("It's {time}".format(time=self.present_time.strftime("%I:%M-%p")))
    return available_menus

class Business:
  def __init__(self, name, franchises):
    self.name = name
    self.franchises = [franchise.address for franchise in franchises]
  def __str__(self):
    return "This is a \"{name}\" businnes, with {franchises} franchises.".format(name=self.name, franchises=self.franchises)

# MENUS
branch = Menu('Branch', {'pancakes': 7.50, 'waffles': 9.00, 'burger': 11.00, 'home fries': 4.50, 'coffee': 1.50, 'espresso': 3.00, 'tea': 1.00, 'mimosa': 10.50, 'orange juice': 3.50}, time(hour=11), time(hour=16))

early_bird = Menu('Early Bird', {'salumeria plate': 8.00, 'salad and breadsticks (serves 2, no refills)': 14.00, 'pizza with quattro formaggi': 9.00, 'duck ragu': 17.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 1.50, 'espresso': 3.00,}, time(hour=15), time(hour=18))

dinner = Menu('Dinner', {'crostini with eggplant caponata': 13.00, 'ceaser salad': 16.00, 'pizza with quattro formaggi': 11.00, 'duck ragu': 19.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 2.00, 'espresso': 3.00,}, time(hour=17), time(hour=23))

kids = Menu('Kids', {'chicken nuggets': 6.50, 'fusilli with wild mushrooms': 12.00, 'apple juice': 3.00}, time(hour=11), time(hour=21))

arepas_menu = Menu('Arepa Menu', {'arepa pabellon': 7.00, 'pernil arepa': 8.50, 'guayanes arepa': 8.00, 'jamon arepa': 7.50}, time(hour=10), time(hour=20))

# FRANCHISES
flagship_store = Franchise("1232 West End Road", [branch, early_bird, dinner, kids])
new_installment = Franchise("12 East Mulberry Street", [branch, early_bird, dinner, kids])
arepas_place = Franchise("189 Fitzgerald Avenue", [arepas_menu])

# BUSINESSES
first_business = Business("Basta Fazoolin' with my Heart", [flagship_store, new_installment])
print(first_business)

second_business = Business("Take a' Arepa", [arepas_place])
print(second_business)


'''
print(branch)
customer_1 = branch.calculate_bill(['pancakes', 'home fries', 'coffee'])
print(customer_1)
print(early_bird)
customer_2 = early_bird.calculate_bill(['salumeria plate', 'mushroom ravioli (vegan)'])
print(customer_2)

flagship_store = Franchise("1232 West End Road", [branch, early_bird, dinner, kids])
print(flagship_store)
new_installment = Franchise("12 East Mulberry Street", [branch, early_bird, dinner, kids])
print(new_installment)
'''