[PL] Przepisownik to projekt, kt�rego celem jest stworzenie aplikacji korzystaj�cej z bazy przepis�w kulinarnych, umo�liwiaj�cej dwie podstawowe funkcjonalno�ci:
1. Wy�wietlanie przepis�w w wybranych kategoriach (system filtrowania przepis�w) - "Mam ochot� na co� konkretnego i id� w�a�nie na zakupy"
2. Wy�wietlanie przepis�w na podstawie dost�pnych sk�adnik�w - "Nie wiem co chc� zje�� / znowu zapomnia�em, �e dzi� niedziela niehandlowa - zobaczmy co mam w lod�wce"

Stan obecny:
Aplikacja konsolowa, umo�liwia wy�wietlenie wybranego przepisu z listy dost�pnych.

Najbli�sze cele:
- System filtrowania przepis�w wed�ug kategorii
- System filtrowania przepis�w wed�ug sk�adnik�w
- Urozmaicenie listy przepis�w
- DO OPANOWANIA: GUI
- Prosty interfejs graficzny
- DO POCZYTANIA: jak wykorzysta� dowoln� internetow� baz� przepis�w? 

Dalsze cele:
- System istotno�ci sk�adnik�w - umo�liwi dodatkow� opcj� rozszerzenia listy wybranych przepis�w o przepisy niewiele odbiegaj�ce od ustawionych filtr�w. Na dob�r dodatkowej
	puli przepis�w wp�ywa� b�dzie istotno�� sk�adnik�w (np. sos chilli cz�sto mo�na pomin��, a makaron ry�owy zast�pi� zwyk�ym makaronem. System sam b�dzie podrzuca�
	propozycje alternatywnych sk�adnik�w.)


[EN] Przepisownik (ang. "Recipator") is project whose goal is to create an application associated with cooking recipes database. It will enable to:
1. Showing recipes by categories - "I am craving [sth] and I'm going shopping"
2. Showing recipes by available ingredients - "I don't know what to eat / the shop is closed now - let's see what i have in the fridge"

Current state:
- Console app, displays recipe chosen by user.

Immediate goals:
- Categories filtering system
- Ingredients filtering system
- More recepies
- TO LEARN: GUI
- Simple GUI
- TO READ: how to use any external online database?

Further goals:
 - Ingredients priority system - enabling additional option to extend the list of chosen recipes. System will consider recipes, which are close to filters settings.
	Selection will depend on ingredients priority (e.g. chilli souce could be ommited and rice noodles could be replaced by regular noodles). System will propose
	alternatice ingredients as well.


