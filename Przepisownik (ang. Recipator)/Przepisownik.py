# -*- coding: utf-8 -*-
"""
Created on Tue May 21 00:39:44 2019

@author: Rege
"""
import recipes
from subprocess import Popen as openrecipe

def start():
    running = 'on'
    while running == 'on':
        print("Witaj w Przepisowniku v1.00!")
        print("W Twojej książce kucharskiej znajduje się " + str(len(list(recipes.recipes))) + " przepisów. \n")
        recipes.show_recipes()
        
        recipe_choice = input("Wpisz numer przepisu, który Cię zainteresował: ")
        if recipe_choice == "exit":
            running = 'off'
        else:
            print()     
            try:
                recipes.show_recipes(int(recipe_choice))
                
                response = input("Aby wywietlić przepis wpisz \"przepis\" w konsoli.")
                if response == "przepis":
                    #opening recipe .pdf file
                    openrecipe(list(list(recipes.recipes.values())[int(recipe_choice)-1].values())[2], shell=True)
                elif response == "exit":
                    running = 'off'
            except IndexError:
                print("W książce nie ma przepisu o numerze " + str(recipe_choice) + ". \n")
            except ValueError:
                print("Podaj numer (zapis cyfrowy). \n")   

start()
print("Zapowiada się wysmienity posiłek :). Do zobaczenia następnym razem!")


