# -*- coding: utf-8 -*-
"""
Created on Tue May 21 00:41:33 2019

@author: Rege
"""

def show_recipes(recipe_index = 0):
    '''Prints info about recipe with recipe number as argument 
    or shows all available recipes and filters in other case'''
    if recipe_index == 0:
        print("Przepisy:")
        for i in range(len(recipes)):
            print("[" + str(i+1) + "] " + list(recipes)[i])
        print()
        print("Filtry:")
        print(list(list(recipes.values())[0]), "\n")
        print()
    else:
        print(list(recipes)[recipe_index-1], "\n")
        print("Składniki:")
        print(list(list(recipes.values())[recipe_index-1].values())[0], "\n")
        print("Kategorie:")
        print(list(list(recipes.values())[recipe_index-1].values())[1], "\n")

recipes = {
 "Ryba w sosie curry z ryżem.": 
    {'składniki': ['ryba','pomidory z puszki', 'mleczko kokosowe', 'pasta curry', 'cebula', 'ryż'], 
     'kategorie': ['curry', 'ryba', 'ryż'], 
     'nazwa pliku': ".\Przepisy\Ryba curry.pdf"}, 
 "Krewetki w sosie curry z makaronem ryżowym i cukinią.": 
      {'składniki': ['makaron ryżowy', 'mleczko kokosowe', 'cukinia', 'pasta curry', 'krewetki'],
       'kategorie': ['curry', 'krewetki', 'makaron'], 
       'nazwa pliku': ".\Przepisy\Makaron ryzowy z cukinia i krewetkami.pdf"},
  "Kurczak orientalny z makaronem ryżowym.":
      {'składniki': ['makaron ryżowy', 'kurczak', 'papryka', 'kapusta pekińska', 'cebula', 'marchew', 'sos sojowy', 'sos chilli'],
       'kategorie': ['kurczak', 'makaron', 'fit'],
       'nazwa pliku': ".\Przepisy\Kurczak orientalny z makaronem ryzowym.pdf"},
  "Tajska zupa curry.":
      {'składniki': ['makaron ryżowy', 'kurczak', 'bulion', 'cebula', 'czosnek', 'limonka', 'pasta curry', 'sos rybny', 'imbir', 'szczypiorek'],
       'kategorie': ['curry', 'zupa'],
       'nazwa pliku': ".\Przepisy\Tajska zupa curry.pdf"},
  "Sałatka krabowa z makaronem i jajkiem.":
      {'składniki': ['paluszki krabowe', 'makaron', 'kukurydza', 'ogórek', 'jajko', 'koperek', 'majonez', 'jogurt'],
       'kategorie': ['paluszki krabowe', 'sałatka', 'makaron'],
       'nazwa pliku': ".\Przepisy\Sałatka krabowa 1.pdf"}
      }

'''
#All recipes (dictionary)
print(recipes, "\n")

#Recipes tags (dict_values)
print(recipes.values(), "\n")

#Recipe tags (dictionary)
print("Tags:")
print(list(recipes.values())[0], "\n")

#Recipe filters (dict_keys)
print(list(recipes.values())[0].keys(), "\n")

#Recipe tags (dict_values)
print(list(recipes.values())[0].values(), "\n")

#Recipes names (list)
print("Recepies:")
print(list(recipes), "\n")

#Recipe ingredients (list)
print("Ingredients:")
print(list(list(recipes.values())[0].values())[0], "\n")

#Recipe categories (list)
print("Categories:")
print(list(list(recipes.values())[0].values())[1], "\n")

#Recipe filters (list):
print("Filters:")
print(list(list(recipes.values())[0]), "\n")
'''