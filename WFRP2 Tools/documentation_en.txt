=====================================
=== [character_creator.py] module ===
=====================================
Module contains classes and methods needed to create a Character Card.
The process of character creation:
	1) Choose name, appearance and character description {TO DO: character description class}
	2) Choose race and draw statistics {done}
	3) Choose class {TO DO}
	4) Choose traits {TO DO}

[CharacterStatistics] class - creates object that contains all the numeric statistics of a character. Takes one argument: 
  - race: 'human', 'elf', 'dwarf' or 'halfling'
  Methods:
    *statistics_drawer() - draws statistics by rolling dices and save them to the values. Depends on chosen race.
    *statictis_writer(scheme) - assigns drawn statistics rolls to main statistics due to the given scheme or archetype. 
				scheme: should be a {string} witch proper archetype name -or- 8 statistics shortcuts separated by commas

===============================
=== [dice_roller.py] module ===
===============================
Module contains functions that enables to carry any possible dice roll.
Functions:
  *dice_roller(n, dice): returns {list} of "n" rolls with "dice" type dice 
  *sum_rolls(list): sum all the rolls from given list
