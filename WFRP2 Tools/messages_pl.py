# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 13:47:30 2019

@author: Rege
"""
        # === CLASS CharacterDescription() ===
# __init__ and __str__
def description_representation(special_features):
    description = """Imię:  {name}
Wiek:  {age}
Rasa:  {race}
    
Opis postaci:
{description}

Cechy charakterystyczne:
"""
    for feature in special_features:
        description += "  - {}\n".format(feature)
    return description



        # === CLASS CharacterStatistics() ===
# __init__ and __str__
empty_rolls = "[aby wylosować statystki musisz najpierw rzucić kośćmi]"
race_error = "W uniwersum Warhammera nie ma takiej rasy jak '{race}'!"

statistics_representation = """
Cechy główne:
    Walka Wręcz          [WW]:  {WS}
    Um. Strzeleckie      [US]:  {BS}
    Krzepa               [K]:   {S}
    Odporność            [Odp]: {T}
    Zręczność            [Zr]:  {Ag}
    Inteligencja         [Int]: {Int}
    Siła Woli            [SW]:  {WP}
    Ogłada               [Ogd]: {Fel}
                                                       
Cechy drugorzędne:
    Ataki                [A]:   {A}
    Żywotność            [Żyw]: {W}
    Siła                 [S]:   {SB}
    Wytrzymałość         [Wt]:  {TB}
    Szybkość             [Sz]:  {M}
    Magia                [Mag]: {Mag}
    Punkty Obłędu        [PO]:  {IP}
    Punkty Przeznaczenia [PP]:  {FP}
    
Seria rzutów na statystyki: {rolls}
"""

# statistic_drawer
statistics_drawer_msg_1 = "Miałeś dziewięc rzutów '2k10' na statystyki główne. Oto Twoje rzuty:"
def statistics_drawer_msg_2(rolls, stat_roll_1, stat_roll_2):
    msg = ""
    for i in range(len(rolls)):
        msg += "  Rzut {index}: {draw}\t({first_roll} + {second_roll})\n".format(index=i+1, draw=rolls[i], first_roll=stat_roll_1[i], second_roll=stat_roll_2[i])
    return msg
statistics_drawer_msg_3 = "Twoja seria rzutów:\t"
statistics_drawer_msg_4 = "Twoje najlepsze rzuty:\t{best_rolls}\nMożesz dowolnie przydzielić (dodać) najlepsze rzuty do statystyk głównych Twojej postaci.\n"
statistics_drawer_msg_5 = "Miałeś po jednym rzucie 'k10' na Żywotność[Żyw] oraz Punkty Przeznaczenia[PP]. Oto Twoje rzuty:"
statistics_drawer_msg_6 = "  Rzut na żywotność:\t{wounds_roll} \tco daje ({wounds_value}) Punktów Życia rasie {race}.\n  Rzut na przeznacznie:\t{fate_roll}\tco daje ({fate_value}) Punktów Przeznaczenia rasie {race}.\n"

# statistic_writer
def statistics_error_message_1(archetypes):
    error_message_1 = """
Wpisz dokładnie 8 skrótów statystyk (po przecinku) lub poprawną nazwę archetypu.
        
"Prawidłowe polskie skróty:"
  [WW] - Walka Wręcz  
  [US] - Umiejętności Strzeleckie 
  [K] - Krzepa
  [Odp] - Odporność
  [Zr] - Zręczność
  [Int] - Inteligencja
  [SW] - Siła Woli 
  [Ogd] - Ogłada
  
"Dostępne archetypy:"
"""        
    for archetype in archetypes:
        error_message_1 += "  *{archetype}: {scheme}\n".format(archetype=archetype, scheme=archetypes[archetype])
    return error_message_1
statistics_error_message_2 = "Nie istnieje taka statystyka jak "
statistics_writer_msg_1 = "Twoja seria rzutów: \t\t{best_rolls}"
statistics_writer_msg_2 = "Twój schemat statystyk: \t{user_scheme}\n"



        # === CONSOLE_APP character_creator.py ===
character_creator_step_1 = "[Krok 1]: Wybór rasy"
character_creator_step_2 = "[Krok 2]: Opis postaci"
character_creator_step_3 = "[Krok 3]: Losowanie statystyk"
character_creator_step_4 = "[Krok 4]: Przypisywanie statystyk"
character_creator_step_5 = "[Krok 5]: Twoja postać jest gotowa!"
choice_error_msg = "Niepoprawny indeks wyboru opcji."
character_creator_msg_1 = "Witaj w kreatorze postaci do systemu Warhammer Fantasy Roleplay 2 ed! Kreator poporawdzi Cię krok po kroku, a na końcu wygeneruje Twoją kartę postaci.\n"
character_creator_msg_2 = "Wybierz swoją rasę:  "
available_races = ['człowiek', 'elf', 'krasnolud', 'niziołek']
character_creator_msg_3 = """Rasa {race} nie jest oficjalnie dostępna w grze. Wciąż jednak możesz jej nieoficjalnie używać. Wybierz jedną z opcji:
    [1] Wybierz jedną z oficjalnych ras.
    [2] Użyj {race} w Karcie Postaci, ale wybierz zestaw zasad jednej z oficjalnych ras.
    
    Oficjalne rasy:
        - człowiek
        - elf
        - krasnilud
        - niziołek
        
    Wybierz opcję:  """
character_creator_msg_4 = """
Wybierz rasę:
    [1] Człowiek
    [2] Elf
    [3] Krasnolud
    [4] Niziołek

    Wybierz opcję:  """
character_creator_msg_5 = "Na twojej karcie postaci w rasie będzie widnieć {race}, ale musisz wybrać obowiązujący dla jednej z oficjalnych ras zestaw zasad." + character_creator_msg_4
character_creator_msg_6 = "Należysz do rasy {user_race}."
character_creator_msg_7 = "Zaczniesz ze statystykami, umiejętnościami etc. przypisanymi rasie {race}."
character_creator_msg_8 = "Jak nazywa się Twoja postać?  "
character_creator_msg_9 = "Ile Twoja postać ma lat?  "
character_creator_msg_10 = "Opisz swoją postać:\n"
character_creator_msg_11 = "Czy Twoja postać ma jakieś znaki szczaególne? (np. tatuaż, hobby czy wyjątkową umiejętność)? Wypisz je, oddzielone znakiem \";\":\n"
character_creator_msg_12 = "Oto opis Twojej postaci:\n"
character_creator_msg_13 = """
Następnym krokiem jest losowanie statystyk. Możliwe akcje:
    [1] Zaakceptuj opis postaci i przejdź do następnego kroku.
    [2] Napisz swój opis postaci od nowa.

    Wybierz opcję:  """
character_creator_msg_14 = "Opis postaci zapisany, przejście do następnego kroku."
character_creatot_msg_15 = """To były Twoje rzuty. Wybierz jedną z opcji:
    [1] Zaakceptuj rzuty i przypisz je do statystyk swojej postaci.
    [2] Spróbuj ponownie.

    Wybierz opcję:  """
character_creator_msg_16 = "Możesz teraz przypisać swoje nalepsze rzuty do statystyk głównych. W tym celu musisz podać listę priorytetów dla swoich statystyk. Użyj skrótów statystyk (oddzielonych przecinkami). Najlepsze rzuty zostaną przydzielone zgodnie z podanym schematem.\n\tprzykład: WW, US, K, Odp, Zr, Int, SW, Ogd"
character_creator_msg_17 = "Podaj schemat priorytetu statystyk:  "
character_creator_msg_18 = """Twoje statystyki zostaną przydzielone w następujący sposób: 
    {statistics}
    
    Opcje do wyboru:
    [1] Zaakceptuj statystyki.
    [2] Podaj inny schemat przydziału statystyk.

    Wybierz opcję:  """