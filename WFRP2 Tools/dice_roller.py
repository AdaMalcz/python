# -*- coding: utf-8 -*-
"""
Part of original Dice Roller code. Includes basic dices operations only.
"""

import random

def dice_roller(number_of_rolls, dice_type):
    rolls = []
    if dice_type == 'd4':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 4))
    elif dice_type == 'd6':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 6))
    elif dice_type == 'd8':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 8))
    elif dice_type == 'd10':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 10))
    elif dice_type == 'd12':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 12))
    elif dice_type == 'd20':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 20))
    elif dice_type == 'd100':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 100))
    else:
        print("There is no '" + dice_type + "' dice.")
    return rolls

def sum_rolls(rolls):
    return sum(rolls)


