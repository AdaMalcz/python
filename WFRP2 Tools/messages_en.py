# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 13:47:33 2019

@author: Rege
"""
        # === CLASS CharacterDescription() ===
# __init__ and __str__
def description_representation(special_features):
    description = """Name:  {name}
Age:   {age}
Race:  {race}
    
Description:
{description}

Special features:
"""
    for feature in special_features:
        description += "  - {}\n".format(feature)
    return description



        # === CLASS CharacterStatistics() ===
# __init__ and __str__
empty_rolls = "[you need to roll some dices first]"
race_error = "There is no '{race}' race in Warhammer Fantasy world!"

statistics_representation = """
Main statistics:
    Weapon Skill      [WS]:  {WS}
    Ballistic Skill   [BS]:  {BS}
    Strength          [S]:   {S}
    Toughness         [T]:   {T}
    Agility           [Ag]:  {Ag}
    Intelligence      [Int]: {Int}
    Will Power        [WP]:  {WP}
    Fellowship        [Fel]: {Fel}
                                                       
Secondary statistics:
    Attacks           [A]:   {A}
    Wounds            [W]:   {W}
    Strength Bonus    [SB]:  {SB}
    Toughness Bonus   [TB]:  {TB}
    Movement          [M]:   {M}
    Magic             [Mag]: {Mag}
    Insanity Points   [IP]:  {IP}
    Fortune Points    [FP]:  {FP}
    
Main statistics rolling serie: {rolls}
"""

# statistic_drawer
statistics_drawer_msg_1 = "You had nine '2d10' rolls for main statistics. These are your rolls:"
def statistics_drawer_msg_2(rolls, stat_roll_1, stat_roll_2):
    msg = ""
    for i in range(len(rolls)):
        msg += "  Roll {index}: {draw}\t({first_roll} + {second_roll})\n".format(index=i+1, draw=rolls[i], first_roll=stat_roll_1[i], second_roll=stat_roll_2[i])
    return msg
statistics_drawer_msg_3 = "The rolling serie is:\t"
statistics_drawer_msg_4 = "Your best rolls are:\t{best_rolls}\nYou can assign (add) best rolls to your main statistics as you want.\n"
statistics_drawer_msg_5 = "You had 'd10' rolls for Wounds[W] and Fate Points[FP]. These are your rolls:"
statistics_drawer_msg_6 = "  Wounds roll:\t{wounds_roll} \twhich gives you ({wounds_value}) Wounds as {race}.\n  Fate roll:\t{fate_roll}\twhich gives you ({fate_value}) Fate Points as {race}.\n"

# statistic_writer
def statistics_error_message_1(archetypes):
    error_message_1 = """
You should pass exactly 8 statistics shortcuts (separated by commas) or the proper archetype name.
        
"Proper english shortcuts:"
  [WS] - Weapon Skill 
  [BS] - Ballistic Skill 
  [S] - Strength 
  [T] - Tougthness 
  [Ag] - Agility 
  [Int] - Intelligence 
  [WP] - Will Power 
  [Fel] - Fellowship
         
"Available archetypes:"
"""        
    for archetype in archetypes:
        error_message_1 += "  *{archetype}: {scheme}\n".format(archetype=archetype, scheme=archetypes[archetype])
    return error_message_1
statistics_error_message_2 = "There is no such a main statistic as "
statistics_writer_msg_1 = "Your rolling serie: \t\t{best_rolls}"
statistics_writer_msg_2 = "Your statistics scheme: \t{user_scheme}\n"



        # === CONSOLE_APP character_creator.py ===
character_creator_step_1 = "[Step 1]: Selecting race"
character_creator_step_2 = "[Step 2]: Describing character"
character_creator_step_3 = "[Step 3]: Drawing statistics"
character_creator_step_4 = "[Step 4]: Assigning statistics"
character_creator_step_5 = "[Step 5]: Your character is ready!"
choice_error_msg = "Uncorrect option choice index."
character_creator_msg_1 = "Welcome to Warhammer Fantasy Roleplay 2 ed. character creator! The creator will guide you step by step and generate your character card."
character_creator_msg_2 = "Select your race:  "
available_races = ['human', 'elf', 'dwarf', 'halfling']
character_creator_msg_3 = """
Race {race} is not available in official rulerbook, but you can still use it unofficially. You can:
    [1] Select one of the official races.
    [2] Use {race} in your Character Card, but choose the set of rules of an official race.

    Official races are:
        - human
        - elf
        - dwarf
        - halfling
        
    Choose an option:  """
character_creator_msg_4 = """
Select race:
    [1] Human
    [2] Elf
    [3] Dwarf
    [4] Halfling

    Choose your option:  """
character_creator_msg_5 = "Your Character Card race will be {race}, but you need to choose a set of rules for your race." + character_creator_msg_4
character_creator_msg_6 = "You are {user_race}."
character_creator_msg_7 = "You will begin with {race} statistics, skills, etc."
character_creator_msg_8 = "What is your character (full) name?  "
character_creator_msg_9 = "What is your character age?  "
character_creator_msg_10 = "Describe your character:\n"
character_creator_msg_11 = "Does your character have any special features (like tattoo, passion or special ability)? Write them down, separated with \";\":\n"
character_creator_msg_12 = "That is your character decription:\n"
character_creator_msg_13 = """
The next step is drawing statistcs. You can:
    [1] Accept character description and go to the next step.
    [2] Write your character description again.

    Choose an option:  """
character_creator_msg_14 = "Character description saved, moving to the next step."
character_creatot_msg_15 = """These were your rolls. You can:
    [1] Accept the rolls and assign them to your character statistics.
    [2] Try once again.

    Choose an option:  """
character_creator_msg_16 = "You can assign your best rolls to your main statistics now. You need to pass your statistics priority list in order to do that. Use statistics shortcuts (separated by comas). The best rolls will be assigned in order described in your scheme.\n\texample: WS, BS, S, T, Ag, Int, WP, Fel"
character_creator_msg_17 = "Pass your statistics priority scheme:  "
character_creator_msg_18 = """These would be your statistics after assignement: 
    {statistics}
    
    You can:
    [1] Accept the statistics.
    [2] Pass another statistcs scheme.

    Choose an option:  """