# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 12:25:15 2019

@author: Rege
"""

import sys
import character_tools as char
from copy import deepcopy
available_languages = ['english', 'polish']

def race_selector(choice, races, error_msg):
    if int(choice) <=4 and int(choice) >=1:
        race = races[int(choice)-1]
        return race
    else:
        raise IndexError(error_msg)
        
def describe_your_character():
    character_name = input(character_creator_msg_8)
    character_age = input(character_creator_msg_9)
    character_description = input(character_creator_msg_10)
    character_special_features = input(character_creator_msg_11)
    user_character_dscrpt = char.CharacterDescription(name=character_name, age=character_age, race=user_race, description=character_description, special_features=character_special_features, language=selected_language)
    print(character_creator_msg_12)
    print(user_character_dscrpt)
    choice_3 = input(character_creator_msg_13)
    if choice_3 == '1':
        print(character_creator_msg_14)
        return user_character_dscrpt
    elif choice_3 == '2':
        describe_your_character()
    else:
        raise IndexError(choice_error_msg)
    return user_character_dscrpt
        
def draw_your_statistics():
    user_character_stats = char.CharacterStatistics(race=race, language=selected_language)
    user_character_stats.statistics_drawer()
    choice_4 = input(character_creatot_msg_15)
    if choice_4 == '1':
        return user_character_stats
    elif choice_4 == '2':
        draw_your_statistics()
    else:
        raise IndexError(choice_error_msg)
    return user_character_stats
        
def assign_your_statistics(statistics):
    statistics_copy = deepcopy(statistics)
    stats_scheme = input(character_creator_msg_17)
    statistics_copy.statistics_writer(stats_scheme)
    #stats_dict = statistics_copy.character_statistics
    choice_5 = input(character_creator_msg_18.format(statistics=statistics_copy))
    if choice_5 == '1':
        return statistics_copy
    elif choice_5 == '2':
        assign_your_statistics(statistics)
    else:
        raise IndexError(choice_error_msg)
    return statistics_copy

# Selecting language (passed as an running argument)
if len(sys.argv) > 2:
    print("\n")
    sys.exit('Select only one language. Available langiages; {}'.format(available_languages))
elif len(sys.argv) == 2 and sys.argv[1] in available_languages:
    selected_language = sys.argv[1]
elif len(sys.argv) == 2 and sys.argv[1] not in available_languages:
    print('Language {} not recognized. English set by default.'.format(sys.argv[1]))
    selected_language = 'english'
else:
    selected_language = 'english'

if selected_language == 'english':
    from messages_en import character_creator_step_1, character_creator_step_2, character_creator_step_3, character_creator_step_4, character_creator_step_5, character_creator_msg_1, character_creator_msg_2, available_races, character_creator_msg_3, character_creator_msg_4, character_creator_msg_5, choice_error_msg, character_creator_msg_6, character_creator_msg_7, character_creator_msg_8, character_creator_msg_9, character_creator_msg_10, character_creator_msg_11, character_creator_msg_12, character_creator_msg_13, character_creator_msg_14, character_creatot_msg_15, character_creator_msg_16, character_creator_msg_17, character_creator_msg_18
elif selected_language == 'polish':
    from messages_pl import character_creator_step_1, character_creator_step_2, character_creator_step_3, character_creator_step_4, character_creator_step_5, character_creator_msg_1, character_creator_msg_2, available_races, character_creator_msg_3, character_creator_msg_4, character_creator_msg_5, choice_error_msg, character_creator_msg_6, character_creator_msg_7, character_creator_msg_8, character_creator_msg_9, character_creator_msg_10, character_creator_msg_11, character_creator_msg_12, character_creator_msg_13, character_creator_msg_14, character_creatot_msg_15, character_creator_msg_16, character_creator_msg_17, character_creator_msg_18

# Welcome message
print()
print(character_creator_msg_1 + "\n")

# Selecting race
print("===================================")
print(character_creator_step_1)
print("===================================\n")
user_race = input(character_creator_msg_2)
if user_race.lower().strip() not in available_races:
    choice = input(character_creator_msg_3.format(race=user_race))
    if choice == "1":
        choice_2 = input(character_creator_msg_4)
        race = race_selector(choice_2, available_races, choice_error_msg)
        user_race = race
    elif choice == "2":
        choice_2 = input(character_creator_msg_5.format(race=user_race))
        race = race_selector(choice_2, available_races, choice_error_msg)
    else:
        raise IndexError(choice_error_msg)
else:
    race = user_race
print()
print(character_creator_msg_6.format(user_race=user_race.title()))
if user_race != race:
    print(character_creator_msg_7.format(race=race.title()))
print()
    
# Describing character
print("===================================")
print(character_creator_step_2)
print("===================================\n")
user_character_description = describe_your_character()
print()

# Drawing statistics
print("===================================")
print(character_creator_step_3)
print("===================================\n") 
user_character_statistics = draw_your_statistics()
#user_character_statistics_copy = user_character_statistics
print()

# Assigning statistics
print("===================================")
print(character_creator_step_4)
print("===================================\n") 
print(user_character_statistics)
print(character_creator_msg_16)
user_character_statistics_assigned = assign_your_statistics(user_character_statistics)
print("===================================")
print(character_creator_step_5)
print("===================================\n") 
print(user_character_description)
print(user_character_statistics_assigned)
print()