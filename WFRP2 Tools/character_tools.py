# -*- coding: utf-8 -*-
"""
Created on Sun Sep  8 16:00:46 2019

@author: Rege
"""

import dice_roller as dice

class CharacterDescription:
    
    def __init__(self, name='', age='00', race='', description='', special_features='', language='english'):
        if language == 'english':
            if name == '':
                name = 'nameless'
            if race == '':
                race = 'ghost'
            if description == '':
                description = 'indescribable'
            if special_features == '':
                special_features = 'both eyes; both legs; both arms'
        elif language == 'polish':
            if name == '':
                name = 'bezimienny'
            if race == '':
                race = 'duch'
            if description == '':
                description = 'nie do opisania'
            if special_features == '':
                special_features = 'dwoje oczu; dwie nogi; dwie ręce'
        else:
            raise KeyError("{language} not found".format(language=language))
            
        self.name = name
        self.age = age
        self.race = race
        self.description = description
        self.special_features = [special_features.split(';')[i].strip().lower() for i in range(len(special_features.split(';')))]
        self.language = language
        
    def __str__(self):
        if self.language == 'english':
            from messages_en import description_representation
        elif self.language == 'polish':
            from messages_pl import description_representation
        else:
            raise KeyError("{language} not found".format(language=self.language))
        
        representation = description_representation(self.special_features).format(name=self.name, age=self.age, race=self.race, description=self.description)
        return representation
    
class CharacterStatistics:
    main_stats = ['WS[WW]', 'BS[US]', 'S[K]', 'T[Odp]', 'Ag[Zr]', 'Int[Int]', 'WP[SW]', 'Fel[Ogd]']
    secondary_stats = ['A[A]', 'W[Żyw]', 'SB[S]', 'TB[Wt]', 'M[Sz]', 'Mag[Mag]', 'IP[PO]', 'FP[PP]']
    races = ['human', 'człowiek', 'elf', 'dwarf', 'krasnolud', 'halfling', 'niziołek']
    human_base = [20, 20, 20, 20, 20, 20, 20, 20]
    elf_base = [20, 30, 20, 30, 20, 20, 20, 20]
    dwarf_base = [30, 20, 20, 30, 10, 20, 20, 10]
    halfling_base = [10, 30, 10, 10, 30, 20, 20, 30]
    secondary_base = [1, 'dice roll', 'first digit of [S]', 'first digit of [T]', 'race-depnded', 0, 0, 'dice roll']
    
    def __init__(self, race='none', language='english'):
        if language == 'english':
            from messages_en import empty_rolls, race_error
        elif language == 'polish':
            self.secondary_base = [1, 'rzut kością', 'pierwsza cyfra [K]', 'pierwsza cyfra [Odp]', 'zależna od rasy', 0, 0, 'rzut kością']
            from messages_pl import empty_rolls, race_error
        else:
            raise KeyError("{language} not found".format(language=language))
            
        self.race = race
        self.statistics_tags = self.main_stats + self.secondary_stats
        self.statistics_values = []
        self.character_statistics = {}
        self.best_rolls = empty_rolls
        self.language = language
        
        #assign basic (race-depended) statistics
        if self.race not in self.races:
            raise KeyError(race_error.format(race=self.race))
        else:
            if race == 'human' or race == 'człowiek':
                self.statistics_values = self.human_base + self.secondary_base
                self.statistics_values[-4] = 4
            elif race == 'elf':
                self.statistics_values = self.elf_base + self.secondary_base
                self.statistics_values[-4] = 5
            elif race == 'dwarf' or race == 'krasnolud':
                self.statistics_values = self.dwarf_base + self.secondary_base
                self.statistics_values[-4] = 3
            else:
                self.statistics_values = self.halfling_base + self.secondary_base
                self.statistics_values[-4] = 4
            self.character_statistics = dict(zip(self.statistics_tags, self.statistics_values))
            

    def __str__(self):
        if self.language == 'english':
            from messages_en import statistics_representation
        elif self.language == 'polish':
            from messages_pl import statistics_representation
        else:
            raise KeyError("{language} not found".format(language=self.language))
            
        representation = statistics_representation.format(WS=self.character_statistics['WS[WW]'], BS=self.character_statistics['BS[US]'], S=self.character_statistics['S[K]'], T=self.character_statistics['T[Odp]'], Ag=self.character_statistics['Ag[Zr]'], Int=self.character_statistics['Int[Int]'], WP=self.character_statistics['WP[SW]'], Fel=self.character_statistics['Fel[Ogd]'], A=self.character_statistics['A[A]'], W=self.character_statistics['W[Żyw]'], SB=self.character_statistics['SB[S]'], TB=self.character_statistics['TB[Wt]'], M=self.character_statistics['M[Sz]'], Mag=self.character_statistics['Mag[Mag]'], IP=self.character_statistics['IP[PO]'], FP=self.character_statistics['FP[PP]'], rolls=self.best_rolls)
        return representation

    def __repr__(self): #NOTE: __str__ is for user - nice looking output info (called by str() or print()); __repr__ is for development & debugging - has all info about an object (called by repr())
        pass

#do all the rolls and player-independent arithmetic operations needed to generate character statistics
#The rules of creating character statistics differs from original rules, but this is a way I prefer myself. There is planned another "vanilla" function for more restrictive players.
    def statistics_drawer(self):
        if self.language == 'english':
            from messages_en import statistics_drawer_msg_1, statistics_drawer_msg_2, statistics_drawer_msg_3, statistics_drawer_msg_4, statistics_drawer_msg_5, statistics_drawer_msg_6
        elif self.language == 'polish':
            from messages_pl import statistics_drawer_msg_1, statistics_drawer_msg_2, statistics_drawer_msg_3, statistics_drawer_msg_4, statistics_drawer_msg_5, statistics_drawer_msg_6
        else:
            raise KeyError("{language} not found".format(language=self.language))
            
    #Drawing Main Statistics 
        stat_roll_1 = dice.dice_roller(9, 'd10')
        stat_roll_2 = dice.dice_roller(9, 'd10')
        rolls = [stat_roll_1[i] + stat_roll_2[i] for i in range(9)]
        rolling_serie=list(reversed(sorted(rolls)))
        self.best_rolls = rolling_serie[:-1]
        
    #Drawing Secondary Statistics
        wounds_roll = dice.dice_roller(1, 'd10')[0]
        fate_roll = dice.dice_roller(1, 'd10')[0]
        if self.race == 'human' or 'człowiek':
            if wounds_roll < 4:
                wounds = 10
            elif wounds_roll < 7:
                wounds = 11
            elif wounds_roll < 10:
                wounds = 12
            else:
                wounds = 13
            if fate_roll < 5:
                fate_points = 2
            else:
                fate_points = 3
        elif self.race == 'elf':
            if wounds_roll < 4:
                wounds = 9
            elif wounds_roll < 7:
                wounds = 10
            elif wounds_roll < 10:
                wounds = 11
            else:
                wounds = 12
            if fate_roll < 5:
                fate_points = 1
            else:
                fate_points = 2
        elif self.race == 'dwarf' or 'krasnolud':
            if wounds_roll < 4:
                wounds = 11
            elif wounds_roll < 7:
                wounds = 12
            elif wounds_roll < 10:
                wounds = 13
            else:
                wounds = 14
            if fate_roll < 5:
                fate_points = 1
            elif fate_roll < 8:
                fate_points = 2
            else:
                fate_points = 3
        else:
            if wounds_roll < 4:
                wounds = 8
            elif wounds_roll < 7:
                wounds = 9
            elif wounds_roll < 10:
                wounds = 10
            else:
                wounds = 11
            if fate_roll < 8:
                fate_points = 2
            else:
                fate_points = 3
        self.character_statistics['W[Żyw]'] = wounds
        self.character_statistics['FP[PP]'] = fate_points
        
        #Printing messages
        print(statistics_drawer_msg_1)
        print(statistics_drawer_msg_2(rolls, stat_roll_1, stat_roll_2))
        print(statistics_drawer_msg_3 + str(rolling_serie))
        print(statistics_drawer_msg_4.format(best_rolls=self.best_rolls))
        print(statistics_drawer_msg_5)
        print(statistics_drawer_msg_6.format(wounds_roll=wounds_roll, wounds_value=wounds, fate_roll=fate_roll, fate_value=fate_points, race=self.race))
        
        #return self.best_rolls, wounds, fate_points

#takes a list of statistics tags in order of priority and assigns statistics values to basic race statistics
#the input should be string with main statistics shortcuts (en or pl) separated by commas
    def statistics_writer(self, stats_priority):
        if self.language == 'english':
            from messages_en import statistics_error_message_1, statistics_error_message_2, statistics_writer_msg_1, statistics_writer_msg_2
        elif self.language == 'polish':
            from messages_pl import statistics_error_message_1, statistics_error_message_2, statistics_writer_msg_1, statistics_writer_msg_2
        else:
            raise KeyError("{language} not found".format(language=self.language))
            
        proper_tags = ['WS', 'WW', 'BS', 'US', 'S', 'K', 'T', 'ODP', 'AG', 'ZR', 'INT', 'WP', 'SW', 'FEL', 'OGD']
        archetypes = { #dictionary of archetypes, each archetype has own pre-defined list of statistics priority
                'whatever': ['WS', 'BS', 'S', 'T', 'AG', 'INT', 'WP', 'FEL']
                } 
        tags_list = stats_priority.split(',')
        if stats_priority in archetypes:
            user_scheme = archetypes[stats_priority]
        else:
            user_scheme = [tags_list[i].strip().upper() for i in range(len(tags_list))]
        
        #summing statistics values with checking input syntax
        if len(user_scheme) != 8:
            raise IndexError(statistics_error_message_1(archetypes))
        else:
            for i in range(len(user_scheme)):
                if user_scheme[i] not in proper_tags:
                        raise KeyError(statistics_error_message_2 + user_scheme[i])
                else:
                    if user_scheme[i] == 'WS' or user_scheme[i] == 'WW':
                        self.character_statistics['WS[WW]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'BS' or user_scheme[i] == 'US':
                        self.character_statistics['BS[US]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'S' or user_scheme[i] == 'K':
                        self.character_statistics['S[K]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'T' or user_scheme[i] == 'ODP':
                        self.character_statistics['T[Odp]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'AG' or user_scheme[i] == 'ZR':
                        self.character_statistics['Ag[Zr]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'INT':
                        self.character_statistics['Int[Int]'] += self.best_rolls[i]
                    elif user_scheme[i] == 'WP' or user_scheme[i] == 'SW':
                        self.character_statistics['WP[SW]'] += self.best_rolls[i]
                    else:
                        self.character_statistics['Fel[Ogd]'] += self.best_rolls[i]
        self.character_statistics['SB[S]'] = int(str(self.character_statistics['S[K]'])[:1])
        self.character_statistics['TB[Wt]'] = int(str(self.character_statistics['T[Odp]'])[:1])
        
        #Printing messages    
        print(statistics_writer_msg_1.format(best_rolls = self.best_rolls))
        print(statistics_writer_msg_2.format(user_scheme = user_scheme))

        #Overwriting rolls list
        if self.language == 'english':
            self.best_rolls = ['points assigned']
        elif self.language == 'polish':
            self.best_rolls = ['punkty przydzielone']
        else:
            self.best_rolls = []
        
        
'''        
### Testing section ###
print("=== CharacterDescription() - ENGLISH VERSION ===")
print("----------------------------------------------------------------------")
print("[1]: Creating character description with no input arguments.")
print("----------------------------------------------------------------------")
character_3 = CharacterDescription()
print(character_3)
print("----------------------------------------------------------------------")
print("[2]: Creating character description with user arguments.")
print("----------------------------------------------------------------------")
character_4 = CharacterDescription(name='Little Tom', age=12, race='halfling', description='Tom is halfling kid, so he is very very small.', special_features='very little ; annoying')
print(character_4)

print("=== CharacterDescription() - WERSJA POLSKA ===")
print("----------------------------------------------------------------------")
print("[1]: Tworzenie opisu postaci bez podania argumentów wejsciowych.")
print("----------------------------------------------------------------------")
character_5 = CharacterDescription(language = 'polish')
print(character_5)
print("----------------------------------------------------------------------")
print("[2]: Tworzenie opisu postaci na podstawie argumentów użytkownika.")
print("----------------------------------------------------------------------")
character_6 = CharacterDescription(name='Wojak Andrzej', age=69, race='człowiek', description='Andrzej lubi wojakować, szczególnie po kilku kuflach "krasnoludzkiego mocnego".', special_features='silny ; mocna głowa', language = 'polish')
print(character_6)



print("=== CharacterStatistics() - ENGLISH VERSION ===")
print("----------------------------------------------------------------------")
print("[1]: Creating character and choosing race. Assigning main statistics based on character race.")
print("----------------------------------------------------------------------")
character = CharacterStatistics('human')
print(character)

print("----------------------------------------------------------------------")
print("[2]: Drawing statistics. Summing rolls values and rejecting worst draw. Assigning wounds and fate points based on corresponding rolls.")
print("----------------------------------------------------------------------")
character.statistics_drawer()
print("----------------------------------------------------------------------")
print("[3]: Accepting drawed statistics.")
print("----------------------------------------------------------------------")
print(character)

print("----------------------------------------------------------------------")
print("[4]: Giving the scheme of rolls-to-main statistics assignment. Calculating secondary statistics based on main statistics.")
print("----------------------------------------------------------------------")
#character.statistics_writer("whatever") #archetype
character.statistics_writer("fel, bs, s, t, ag, int, wp, ws") #correct scheme
#character.statistics_writer("ws, bs, s, t, ag, int, w") #uncorrect scheme
print("----------------------------------------------------------------------")
print("[5]: Starting statistics set is completed!")
print("----------------------------------------------------------------------")
print(character)

print("=== CharacterStatistics() - WERSJA POLSKA ===")
print("----------------------------------------------------------------------")
print("[1]: Tworzę postać i wybieram rasę. Automatyczne przypisanie bazowych statystyk na podstawie rasy postaci.")
print("----------------------------------------------------------------------")
character_2 = CharacterStatistics('człowiek', 'polish')
print(character_2)

print("----------------------------------------------------------------------")
print("[2]: Losuję statystyki. Automatyczne zsumowanie rzutów i odrzucenie najgorszego wyniku. Automatyczne przydzielenie życia i punktów przeznaczenia na podstawie rzutów.")
print("----------------------------------------------------------------------")
character_2.statistics_drawer()
print("----------------------------------------------------------------------")
print("[3]: Akceptuję zestaw wylosowanych statystyk.")
print("----------------------------------------------------------------------")
print(character_2)

print("----------------------------------------------------------------------")
print("[4]: Podaję schemat rozdzielenia wylosowanego zestawu statystyk. Automatyczne przydzielenie rzutów do odpowiednich statystyk według schematu. Automatyczne przeliczenie cech drugorzędnych zależnych od wartosci cech głównych.")
print("----------------------------------------------------------------------")
#character_2.statistics_writer("whatever") #archetype
character_2.statistics_writer("ogd, us, k, odp, zr, int, sw, ww") #correct scheme
#character_2.statistics_writer("ogd, us, k, odp, zr, int, sw") #uncorrect scheme
print("----------------------------------------------------------------------")
print("[5]: Zestaw statystyk początkowych został skompletowany!")
print("----------------------------------------------------------------------")
print(character_2)

"""
TO DO:
    *NEW FILE: session.py:
        - console app
        - save and load character statistics
        - save and load campaign
        - campaign game log
    *skills_and_talents
    *equipment
    *experience_counter
    *character_card 
    *GUI app
"""
'''            