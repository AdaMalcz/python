[EN] The main goal of that application is to speed up a gamplay of Warhammer Fantasy Roleplay 2ed. There is a lot of rolling dices in the game and players 
     need to check multiple tables or include some modificators to complete an action or create a character. Warhammer Tools performs all the calculations
     for player, allowing him to focus on the roleplay, which is the main essence of the game.

TO DO:
*name_and_appearance_and_description
*skills_and_talents
*equipment
*experience_counter
*character_card 
*NEW FILE: session.py:
  - console app
  - save and load character statistics
  - save and load campaign
  - campaign game log
*GUI app

===========================================================================================================================================================

[PL] G��wnym celem aplikacji jest przy�pieszenie rozgrywki w systemie Warhammer Fantasy Roleplay 2ed. W grze gracze zmuszeni s� do wielokrotnych rzut�w
     ko��mi, por�wnywania ich wynik�w z licznymi tabelami oraz uwzgl�dniania r�znorakich modyfikator�w w celu wykonania akcji b�dz stworzenia postaci. 
     Warhammer Tools przeprowadza wszelkie obliczenia za gracza, pozwalaj�c mu skupi� si� na aspekcie odgrywania postaci, kt�ry jest esencj� tej gry.

DO ZROBIENIA:
*opis postaci (imi�, wygl�d etc.)
*umiej�tno�ci i zdolno�ci
*ekwipunek
*system zliczania punkt�w do�wiadczenia
*karta postaci
*NEW FILE: session.py: (nowy plik - sesja rpg)
  - aplikacja konsolowa
  - zapisywanie i wczytywanie statystyk postaci
  - zapisywanie i wczytywanie post�pu kampanii
  - dziennik kampanii
*interfejs graficzny