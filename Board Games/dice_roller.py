# -*- coding: utf-8 -*-
"""
Created on Sat Sep  7 19:00:16 2019

@author: Rege
"""

import random

def dice_roller(number_of_rolls, dice_type):
    rolls = []
    if dice_type == 'k4':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 4))
    elif dice_type == 'k6':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 6))
    elif dice_type == 'k8':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 8))
    elif dice_type == 'k10':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 10))
    elif dice_type == 'k12':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 12))
    elif dice_type == 'k20':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 20))
    elif dice_type == 'k100':
        for i in range(number_of_rolls):
            rolls.append(random.randint(1, 100))
    else:
        print("There is no '" + dice_type + "' dice.")
    return rolls

def sum_rolls(rolls):
    return sum(rolls)

#Function returns statistics of character for Warhammer Fantasy Roleplay 2 edition - one command instead of dozens of rolls
#The rule for drawing main stats is [you get one extra roll and reject the worst] - it slightly differs from original rules
def wfrp2_stats(race='none'):
    main_stats = ['WS[WW]', 'BS[US]', 'S[K]', 'T[Odp]', 'Ag[Zr]', 'Int[Int]', 'WP[SW]', 'Fel[Ogd]']
    secondary_stats = ['A[A]', 'W[Żyw]', 'SB[S]', 'TB[Wt]', 'M[Sz]', 'Mag[Mag]', 'IP[PO]', 'FP[PP]']
    races = ['human', 'elf', 'dwarf', 'halfling']
    
    #drawing basic stats
    stat_roll_1 = dice_roller(9, 'k10')
    stat_roll_2 = dice_roller(9, 'k10')
    rolls = [stat_roll_1[i] + stat_roll_2[i] for i in range(9)]
    rolling_serie=list(reversed(sorted(rolls)))
    print("You had nine '2k10' rolls. These are your rolls:")
    for i in range(len(rolls)):
        print("  Roll {index}: {draw}\t({first_roll} + {second_roll})".format(index=i+1, draw=rolls[i], first_roll=stat_roll_1[i], second_roll=stat_roll_2[i]))
    print()
    print("The rolling serie is:\t" + str(rolling_serie))
    rolling_serie = rolling_serie[:-1]
    print("Your best rolls are:\t" + str(rolling_serie) + ".\nYou can assign them (add) to your basic statistics as you want.")
    print()
    
    ### Race-dependent stats ###
    if race == "none":
        pass
    elif race not in races:
        print("There are no \"{race}\" race in the world of Warhammer Fantasy!".format(race=race))
    else:
        human_base = [20, 20, 20, 20, 20, 20, 20, 20]
        elf_base = [20, 30, 20, 30, 20, 20, 20, 20]
        dwarf_base = [30, 20, 20, 30, 10, 20, 20, 10]
        halfling_base = [10, 30, 10, 10, 30, 20, 20, 30]
    
    #drawing secondary stats
        wounds_roll = dice_roller(1, 'k10')[0]
        fate_roll = dice_roller(1, 'k10')[0]
        basic_stats = {}
        move_speed = 0
        wounds = 0
        fate_points = 0
        if race == "human":
            basic_stats = dict(zip(main_stats, human_base))
            move_speed = 4
            if wounds_roll < 4:
                wounds = 10
            elif wounds_roll < 7:
                wounds = 11
            elif wounds_roll < 10:
                wounds = 12
            else:
                wounds = 13
            if fate_roll < 5:
                fate_points = 2
            else:
                fate_points = 3
        elif race == "elf":
            basic_stats = dict(zip(main_stats, elf_base))
            move_speed = 5
            if wounds_roll < 4:
                wounds = 9
            elif wounds_roll < 7:
                wounds = 10
            elif wounds_roll < 10:
                wounds = 11
            else:
                wounds = 12
            if fate_roll < 5:
                fate_points = 1
            else:
                fate_points = 2
        elif race == "dwarf":
            basic_stats = dict(zip(main_stats, dwarf_base))
            move_speed = 3
            if wounds_roll < 4:
                wounds = 11
            elif wounds_roll < 7:
                wounds = 12
            elif wounds_roll < 10:
                wounds = 13
            else:
                wounds = 14
            if fate_roll < 5:
                fate_points = 1
            elif fate_roll < 8:
                fate_points = 2
            else:
                fate_points = 3
        elif race == "halfling":
            basic_stats = dict(zip(main_stats, halfling_base))
            move_speed = 4
            if wounds_roll < 4:
                wounds = 8
            elif wounds_roll < 7:
                wounds = 9
            elif wounds_roll < 10:
                wounds = 10
            else:
                wounds = 11
            if fate_roll < 8:
                fate_points = 2
            else:
                fate_points = 3
        print()
        print("You are a {race}. \n  Your basic statistics are: {basic_stats}. \n  Your [W](wounds) roll was {wounds_roll} which gives you {wounds_value} wounds. \n  Your [FP](fate points) roll was {fate_roll} which gives you {fate_value} fate points. \n  Your [M](movement speed) is {speed}.".format(race=race, basic_stats=basic_stats, wounds_roll=wounds_roll, wounds_value=wounds, fate_roll=fate_roll, fate_value=fate_points, speed=move_speed)) 
        print()
        print("Your [SB](strength bonus) and [TB](toughness bonus) depends on the first digits of [T](toughness) and [Ag](agility) - accordingly.")
        print()
        print("At the very beginning your [Mag](magic) and [IP](Insanity points) are always 0.")

### TESTING SECTION ###
    
#dice rolls
print("DICE ROLLS")
rolls_serie = dice_roller(4, 'k6')
print("The rolls serie is:")
print(rolls_serie)
print("All rolls sum to " + str(sum_rolls(rolls_serie)) + ".")
print()

#warhammer statistics
print("WARHAMMER")
wfrp2_stats()
print()
wfrp2_stats('human')
print()
wfrp2_stats('elf')
print()
wfrp2_stats('dwarf')
print()
wfrp2_stats('halfling')
print()
wfrp2_stats('spiderman')
print()

'''
TO DO:
    * finish wfrp2_stats function
        - for concole app: assembling draws with basic statistics
        - for concole app: returning starting stats dictionary
        !!!wfrp2_stats function is now part of "WFRP2 Tools" project and it could be no longer developed in dice_roller file (this file)!!!
    * make it console app
    * give it some GUI
    * think about more ways to extend this
'''