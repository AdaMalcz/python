alphabet = "abcdefghijklmnopqrstuvwxyz"
message = "dfc jhjj ifyh yf hrfgiv xulk? vmph bfzo! qtl eeh gvkszlfl yyvww kpi hpuvzx dl tzcgrywrxll!"
keyword = "friends"

def vigenere_decoder(message, keyword):
    key = ""
    decoded_message = ""

    #generating key for message
    for i in range(len(message)):
        key += keyword[i % len(keyword)]
    
    #shifting letters in message
    for i in range(len(message)):
        if message[i] in alphabet:
            decoded_message += alphabet[(alphabet.find(message[i]) - alphabet.find(key[i])) % 26 ]
        else:
            decoded_message += message[i]    
    return decoded_message
message = "you were able to decode this? nice work! you are becoming quite the expert at crytography!"
keyword = "friends"

def vigenere_coder(message, keyword):
    key = ""
    coded_message = ""

    #generating key for message
    for i in range(len(message)):
        key += keyword[i % len(keyword)]
    
    #shifting letters in message
    for i in range(len(message)):
        if message[i] in alphabet:
            coded_message += alphabet[(alphabet.find(message[i]) + alphabet.find(key[i])) % 26 ]
        else:
            coded_message += message[i]    
    return coded_message

print(vigenere_coder(message, keyword))
decoded_message = vigenere_decoder(message, keyword)
print("Message: " + message + "\n")
print("Keyword: " + keyword + "\n")
print("Decoded message: " + decoded_message)

'''
[ALTERNATE SOLUTION FOR GENERATING KEY]

    keyword_repeated = ''
    while len(keyword_repeated) < len(coded_message):
        keyword_repeated += keyword
    keyword_final = keyword_repeated[0:len(coded_message)]
'''