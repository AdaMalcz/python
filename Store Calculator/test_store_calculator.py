# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 01:23:52 2019

@author: Rege
"""

import unittest
import store_calculator as app

#Test store variable
test_store = {
    "name": "Imaginary ITems Store",
    "description": "This store doesn't exist but the prices are unbeatable!",
    "items": ["Ilustrated Python Documentation", "Some Super Computer", "Nerdy Glasses", "Cool Software"],
    "prices": [12.99, 1457.00, 9.50, 35.00], #prices in $
    "sales": {
            "week_1": [4, 0, 2, 12],
            "week_2": [6, 2, 2, 4],
            "week_3": [5, 1, 3, 8],
            "week_4": [10, 1, 0, 3],
            "week_5": [4, 2, 3, 3]}
        }

class revenueTest(unittest.TestCase):
    def test_weekly_equals_3099_46(self): #4x12.99 + 2x1457.00 + 3x9.50 + 3x35.00 = 3099.46 (week_5)
        self.assertAlmostEqual(app.revenue(test_store, 'weekly'), 3099.46, 2)
    def test_monthly_equals_9722_75(self): #week_5 + 10x12.99 + 1x1457.00 + 0 + 3x35.00 = 1691.9 (week_4) + 5x12.99 + 1x1457.00 + 3x9.50 + 8x35.00 = 1830.45 (week_3) + 6x12.99 + 2x1457.00 + 2x9.50 + 4x35.00 = 3150.94 (week_2) = 9772.75
        self.assertAlmostEqual(app.revenue(test_store, 'monthly'), 9772.75, 2)
    def test_total_equals_10263_71(self): #4x12.99 + 0 + 2x9.50 + 12x35.00 = 490.96 (week_1) + week_2 + week_3 + week_4 + week_5 = 490.96 + 9772.75 = 10263.71
        self.assertAlmostEqual(app.revenue(test_store, 'total'), 10263.71, 2)
    def test_average_equals_2052_74(self): #10263.71/5 = 2052.742
        self.assertAlmostEqual(app.revenue(test_store, 'average'), 2052.74, 2)
    
if __name__ == '__main__':
    unittest.main()