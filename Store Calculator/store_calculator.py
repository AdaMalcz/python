# -*- coding: utf-8 -*-
"""
Store Calculator is a bunch of functions which can be used in any kind of 
store to calculate income and provide statistics about sales.
"""

#Calculates lowest, highest and avarege price
def prices(prices_list):
    lowest_price = sorted(prices_list)[0]
    highest_price = sorted(prices_list)[-1]
    average_price = sum(prices_list)/len(prices_list)
    return lowest_price, highest_price, average_price

#Change prices by given perecentage value 
def change_prices(prices_list, perecentage_change, mode='show'):
    new_prices = [price + price*perecentage_change/100 for price in prices_list]
    if mode == 'show':
        print("New would be: " + str(new_prices))
    elif mode == 'apply':
        print("New prices are: " + str(new_prices))
        return new_prices
    else: 
        print("There are only 'show' and 'apply' modes!")

#Displays weekly, monthly, total or avarege revenue
def revenue(store_variable, mode='weekly'):
    income = 0
    if mode == 'weekly':
        for i in range(len(store_variable.get("prices"))):
            income += list(store_variable.get("sales").values())[-1][i] * store_variable.get("prices")[i]
        print("The income for last week is " + str(round(income, 2)) + "$")
    elif mode == 'monthly':
        for week in range(len(list(store_variable.get("sales").values()))-4, len(list(store_variable.get("sales").values()))):
            for i in range(len(store_variable.get("prices"))):
                income += list(store_variable.get("sales").values())[week][i] * store_variable.get("prices")[i]
        print("The income for last month is " + str(round(income, 2)) + "$")
    elif mode == 'total':
        for week in range(len(list(store_variable.get("sales").values()))):
            for i in range(len(store_variable.get("prices"))):
                income += list(store_variable.get("sales").values())[week][i] * store_variable.get("prices")[i]
        print("The total income is " + str(round(income, 2)) + "$")
    elif mode == 'average':
        weeks = len(list(store_variable.get("sales").values()))
        for week in range(len(list(store_variable.get("sales").values()))):
            for i in range(len(store_variable.get("prices"))):
                income += list(store_variable.get("sales").values())[week][i] * store_variable.get("prices")[i]
        print("The average income is " + str(round(income/weeks, 2)) + "$")
        return income/weeks
    else:
        print("There are only 'weekly', 'monthly', 'total' and 'avarage' modes!")
    return income
            

"""
TO DO:
* return the name of cheapest/most expensive item
* day by day store operating application (console)
    - saving daily sales info into store variable



some old code:
    test_prices = [4, 3, 7, 5]
    a,b,c = prices(test_prices)
    print(c)
    
    #Test store variable
test_store = {
    "name": "Imaginary ITems Store",
    "description": "This store doesn't exist but the prices are unbeatable!",
    "items": ["Ilustrated Python Documentation", "Some Super Computer", "Nerdy Glasses", "Cool Software"],
    "prices": [12.99, 1457.00, 9.50, 35.00], #prices in $
    "sales": {
            "week_1": [4, 0, 2, 12],
            "week_2": [6, 2, 2, 4],
            "week_3": [5, 1, 3, 8],
            "week_4": [10, 1, 0, 3],
            "week_5": [4, 2, 3, 3]}
        }

#testing code
test_weekly_revenue = revenue(test_store, 'weekly')
#print(test_weekly_revenue)

test_monthly_revenue = revenue(test_store, 'monthly')
#print(test_monthly_revenue)

test_total_revenue = revenue(test_store, 'total')
#print(test_total_revenue)

test_average_revenue = revenue(test_store, 'average')
#print(test_average_revenue)

"""