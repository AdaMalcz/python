# -*- coding: utf-8 -*-
"""
Created on Mon May  6 16:34:58 2019

@author: Rege
"""
alphabet = "abcdefghijklmnopqrstuvwxyz"

#Caesar Cipher
def caesar_coder(message, offset):
    decoded_message = message
    coded_message = "" 

    for letter in decoded_message:
        if letter in alphabet:
            coded_message += alphabet[(alphabet.find(letter) - offset) % 26 ]
        else:
            coded_message += letter
    return coded_message
    
def caesar_decoder(message, offset):
    coded_message = message
    decoded_message = "" 

    for letter in coded_message:
        if letter in alphabet:
            decoded_message += alphabet[(alphabet.find(letter) + offset) % 26 ]
        else:
            decoded_message += letter
    return decoded_message

def caesar_info():
    print('''
The Caesar Cipher is monoalphabetic substitution cipher. Here's how it works: 
You take your message, something like "hello" and then you shift all of the 
letters by a certain offset. For example, if I chose an offset of 3 and a 
message of "hello", I would code my message by shifting each letter 3 places
to the left (with respect to the alphabet). So "h" becomes "e", "e" becomes, 
"b", "l" becomes "i", and "o" becomes "l". Then I have my coded message,"ebiil"!
''')

#Vigenère Cipher
def vigenere_coder(message, keyword):
    key = ""
    coded_message = ""

    #generating key for message
    for i in range(len(message)):
        key += keyword[i % len(keyword)]
    
    #shifting letters in message
    for i in range(len(message)):
        if message[i] in alphabet:
            coded_message += alphabet[(alphabet.find(message[i]) + alphabet.find(key[i])) % 26 ]
        else:
            coded_message += message[i]    
    return coded_message

def vigenere_decoder(message, keyword):
    key = ""
    decoded_message = ""

    #generating key for message
    for i in range(len(message)):
        key += keyword[i % len(keyword)]
    
    #shifting letters in message
    for i in range(len(message)):
        if message[i] in alphabet:
            decoded_message += alphabet[(alphabet.find(message[i]) - alphabet.find(key[i])) % 26 ]
        else:
            decoded_message += message[i]    
    return decoded_message

def vigenere_info():
    print('''
The Vigenère Cipher is a polyalphabetic substitution cipher. What this means 
is that opposed to having a single shift that is applied to every letter, the 
Vigenère Cipher has a different shift for each individual letter. The value of 
the shift for each letter is determined by a given keyword.

Consider the message: "barryisthespy"

If we want to code this message, first we choose a keyword. For this example, 
we'll use the keyword "dog".

Now we use the repeat the keyword over and over to generate a keyword phrase
that is the same length as the message we want to code: "dogdogdogdogd". 

We shift the each letter of our message by the place value of the corresponding 
letter in the keyword phrase, assuming that "a" has a place value of 0, "b" 
has a place value of 1. So we shift "b", which has an index of 1, by the index 
of "d", which is 3. This gives us an place value of 4, which is "e", and so on.

message:                    b  a  r  r  y  i  s  t  h  e  s  p  y
keyword phrase:             d  o  g  d  o  g  d  o  g  d  o  g  d
resulting place value:      4  14 15 12 16 24 11 21 25 22 22 17 5
coded message:              e  o  x  u  m  o  v  h  n  h  g  v  b
''')